package com.piopanjaitan.pokedex

data class Pokemon(
    var name: String? = null,
    var detail: String? = null,
    var photo: Int = 0,
    var type: String? = null,
    var height: String? = null,
    var weight: String? = null,
    var ability: String? = null,
    var ability_detail: String? = null,
    var ability2: String? = null,
    var ability2_detail: String? = null,
)
