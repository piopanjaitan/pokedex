package com.piopanjaitan.pokedex

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ListPokemonAdapter(private val listPokemon: ArrayList<Pokemon>) : RecyclerView.Adapter<ListPokemonAdapter.ListViewHolder>() {

    private lateinit var  onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.tv_item_name)
        var tvDetail: TextView = itemView.findViewById(R.id.tv_item_detail)
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_item_photo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_row_pokemon, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val pokemon = listPokemon[position]

        Glide.with(holder.itemView.context)
            .load(pokemon.photo)
            .apply(RequestOptions().override(95, 95))
            .into(holder.imgPhoto)

        holder.tvName.text = pokemon.name
        holder.tvDetail.text = pokemon.detail
        holder.itemView.setOnClickListener {
            onItemClickCallback.onItemClicked(listPokemon[holder.absoluteAdapterPosition])
        }
    }

    override fun getItemCount(): Int {
        return listPokemon.size
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Pokemon)
    }
}