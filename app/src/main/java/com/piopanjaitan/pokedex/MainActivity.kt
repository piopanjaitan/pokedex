package com.piopanjaitan.pokedex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(), ListPokemonAdapter.OnItemClickCallback {
    private lateinit var rvPokemon: RecyclerView
    private var title: String = "List Pokemon"
    private var list: ArrayList<Pokemon> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setActionBarTitle(title)

        rvPokemon = findViewById(R.id.rv_pokemons)
        rvPokemon.setHasFixedSize(true)

        list.addAll(PokemonsData.listData)
        showRecyclerList()
    }

    private fun showRecyclerList() {
        rvPokemon.layoutManager = LinearLayoutManager(this)
        val listPokemonAdapter = ListPokemonAdapter(list)
        rvPokemon.adapter = listPokemonAdapter

        listPokemonAdapter.setOnItemClickCallback(object : ListPokemonAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Pokemon) {
                showSelectedPokemon(data)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setMode(selectedMode: Int) {
        when (selectedMode) {
            R.id.about_page -> {
                title = "About Creator"
                val aboutIntent = Intent(this@MainActivity, About::class.java)
                startActivity(aboutIntent)
            }
        }
    }

    private fun showSelectedPokemon(pokemon: Pokemon) {
        title = "Pokemon Detail"
        Toast.makeText(this, "I choose you, " + pokemon.name, Toast.LENGTH_SHORT).show()

        val moveWithDataIntent = Intent(this, SelectedPokemon::class.java)
        moveWithDataIntent.putExtra("pokemonNames", pokemon.name)
        moveWithDataIntent.putExtra("pokemonDetails", pokemon.detail)
        moveWithDataIntent.putExtra("pokemonImages", pokemon.photo)
        moveWithDataIntent.putExtra("pokemonTypes", pokemon.type)
        moveWithDataIntent.putExtra("pokemonHeights", pokemon.height)
        moveWithDataIntent.putExtra("pokemonWeights", pokemon.weight)
        moveWithDataIntent.putExtra("pokemonAbilities", pokemon.ability)
        moveWithDataIntent.putExtra("pokemonAbilitiesDetail", pokemon.ability_detail)
        moveWithDataIntent.putExtra("pokemonAbilities2", pokemon.ability2)
        moveWithDataIntent.putExtra("pokemonAbilities2Detail", pokemon.ability2_detail)
        startActivity(moveWithDataIntent)

    }

    override fun onItemClicked(data: Pokemon) {
        startActivity(Intent(this,SelectedPokemon::class.java).putExtra("pokemonNames",PokemonsData.listData))
    }

    private fun setActionBarTitle(title: String) {
        supportActionBar?.title = title
    }
}