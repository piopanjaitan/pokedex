package com.piopanjaitan.pokedex

import android.graphics.drawable.Drawable
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyCharacterMap.load
import android.view.PointerIcon.load
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.PointerIconCompat.load
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.lang.System.load

class SelectedPokemon : AppCompatActivity() {

    private var title: String = "Pokemon Detail"

    private lateinit var tvName: TextView
    private lateinit var tvDetail: TextView
    private lateinit var imgPhoto: ImageView
    private lateinit var tv_pokemons_type: TextView
    private lateinit var tv_pokemons_height: TextView
    private lateinit var tv_pokemons_weight: TextView
    private lateinit var tv_item_ability: TextView
    private lateinit var tv_item_ability_detail: TextView
    private lateinit var tv_item_ability_2: TextView
    private lateinit var tv_item_ability_2_detail: TextView
    private lateinit var btn_action_share: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selected_pokemon)
        setActionBarTitle(title)
        initData();
    }

    private fun initData() {

        tvName = findViewById(R.id.tv_item_name)
        tvDetail = findViewById(R.id.tv_item_detail)
        imgPhoto = findViewById(R.id.img_item_photo)
        tv_pokemons_type = findViewById(R.id.tv_pokemons_type)
        tv_pokemons_height = findViewById(R.id.tv_pokemons_height)
        tv_pokemons_weight = findViewById(R.id.tv_pokemons_weight)
        tv_item_ability = findViewById(R.id.tv_item_ability)
        tv_item_ability_detail = findViewById(R.id.tv_item_ability_detail)
        tv_item_ability_2 = findViewById(R.id.tv_item_ability_2)
        tv_item_ability_2_detail = findViewById(R.id.tv_item_ability_2_detail)
        btn_action_share = findViewById(R.id.btn_action_share)
        getData()
    }

    private fun getData() {
        var intent = intent.extras

        var pokemonNames = intent!!.getString("pokemonNames")
        var pokemonDetails = intent!!.getString("pokemonDetails")
        var pokemonImages = intent!!.get("pokemonImages")
        var pokemonTypes = intent!!.getString("pokemonTypes")
        var pokemonHeights = intent!!.getString("pokemonHeights")
        var pokemonWeights = intent!!.getString("pokemonWeights")
        var pokemonAbilities = intent!!.getString("pokemonAbilities")
        var pokemonAbilitiesDetail = intent!!.getString("pokemonAbilitiesDetail")
        var pokemonAbilities2 = intent!!.getString("pokemonAbilities2")
        var pokemonAbilities2Detail = intent!!.getString("pokemonAbilities2Detail")


        tvName.text = pokemonNames
        tvDetail.text = pokemonDetails
        imgPhoto.setImageResource(pokemonImages as Int)
        tv_pokemons_type.text = pokemonTypes
        tv_pokemons_height.text = pokemonHeights
        tv_pokemons_weight.text = pokemonWeights
        tv_item_ability.text = pokemonAbilities
        tv_item_ability_detail.text = pokemonAbilitiesDetail
        tv_item_ability_2.text = pokemonAbilities2
        tv_item_ability_2_detail.text =pokemonAbilities2Detail

        btn_action_share.setOnClickListener{
            Toast.makeText(this, "Share $pokemonNames", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setActionBarTitle(title: String) {
        supportActionBar?.title = title
    }
}