package com.piopanjaitan.pokedex

object PokemonsData {
    private val pokemonNames = arrayOf(
        "Bulbasaur",
        "Charmander",
        "Squirtle",
        "Pikachu",
        "Psyduck",
        "Meowth",
        "Jigglypuff",
        "Cubone",
        "Eevee",
        "Magikarp")

    private val pokemonDetails = arrayOf(
        "Known as the Seed Pokémon, Bulbasaur can survive for days solely on sunlight. It likes to take a nap in the sunshine. While it sleeps, the seed on its back catches the rays and uses the energy to grow.",
        "Charmander are small, bipedal lizard-like Pokémon native to Kanto. They have blue eyes, orange skin, three-toed clawed feet, yellow bellies, and a single yellow pad covering most of the soles of their feet.",
        "Squirtle, known as the Tiny Turtle Pokémon, are turtle Pokémon with large eyes and chubby cheeks, capable of moving either on two feet or on all fours. Their skin is a light blue, and they possess a long, curled tail.",
        "Pikachu were the first Electric-type Pokémon created, their design intended to revolve around the concept of electricity.",
        "Known as the duck Pokémon, Psyduck is constantly stunned by its headache, and usually just stands vacantly, trying to calm itself.",
        "Known as the Scratch Cat Pokémon, it spends most of the daytime sleeping and prowls the city streets at night.",
        "Known as the Balloon Pokémon, Jigglypuff evolves from Igglybuff when it reaches a certain point of happiness, and evolves into Wigglytuff when exposed to a Moon Stone. ",
        "Cubone wears the skull of its dead mother as a helmet, and is known as the \"Lonely Pokémon\" because of its tendency to keep to itself and avoid social situations, due to the trauma caused by the death of its mother.",
        "Known as the Evolution Pokémon in the games and the anime, Eevee has an unstable genetic code, which allows it to evolve into one of eight different Pokémon, known as Eeveelutions, depending on the situation",
        "Known as the Fish Pokémon, Magikarp is found in many bodies of water, especially lakes, rivers, and ponds. Magikarp received mixed responses from critics, with some criticism directed towards it for being useless or pointless. ")

    private val pokemonImages = intArrayOf(
        R.drawable.bulbasaur,
        R.drawable.charmander,
        R.drawable.squirtle,
        R.drawable.pikachu,
        R.drawable.psyduck,
        R.drawable.meowth,
        R.drawable.jigglypuff,
        R.drawable.cubone,
        R.drawable.eevee,
        R.drawable.magikarp)

    private val pokemonTypes = arrayOf(
        "Grass / Poison",
        "Fire",
        "Water",
        "Electric",
        "Water",
        "Normal",
        "Normal / Fairy",
        "Ground",
        "Normal",
        "Water"
    )

    private val pokemonHeights = arrayOf(
        "0.7 m",
        "0.6 m",
        "0.5 m",
        "0.4 m",
        "0.8 m",
        "0.4 m",
        "0.5 m",
        "0.4 m",
        "0.3 m",
        "0.9 m"
    )

    private val pokemonWeights = arrayOf(
        "6.9 Kg",
        "8.5 Kg",
        "9 Kg",
        "6 kg",
        "19.6 kg",
        "4.2 kg",
        "5.5 kg",
        "6.5 kg",
        "6.5 kg",
        "10 kg"
    )

    private val pokemonAbilities = arrayOf(
        "- Overgrow",
        "- Blaze",
        "- Torrent",
        "- Static",
        "- Damp",
        "- Pickup",
        "- Cute Charm",
        "- Rock Head",
        "- Run Away",
        "- Swift Swim"
    )

    private val pokemonAbilitiesDetail = arrayOf(
        "Strengthens grass moves to inflict 1.5× damage at 1/3 max HP or less.",
        "Strengthens fire moves to inflict 1.5× damage at 1/3 max HP or less.",
        "Strengthens water moves to inflict 1.5× damage at 1/3 max HP or less.",
        "Has a 30% chance of paralyzing attacking Pokémon on contact.",
        "Prevents self destruct, explosion, and aftermath from working while the Pokémon is in battle.",
        "Picks up other Pokémon's used and Flung held items. May also pick up an item after battle.",
        "Has a 30% chance of infatuating attacking Pokémon on contact.",
        "Protects against recoil damage.",
        "Ensures success fleeing from wild battles.",
        "Doubles Speed during rain."
    )

    private val pokemonAbilities2 = arrayOf(
        "-Chlorophyll",
        "- Solar Power",
        "- Rain Dish",
        "- Lightning Rod",
        "- Cloud Nine",
        "- Technician",
        "- Competitive",
        "- Lightning Rod",
        "- Adaptability",
        "- Rattled"
    )

    private val pokemonAbilities2Detail = arrayOf(
        "Doubles Speed during strong sunlight.",
        "Increases Special Attack to 1.5× but costs 1/8 max HP after each turn during strong sunlight.",
        "Heals for 1/16 max HP after each turn during rain.",
        "Redirects single-target electric moves to this Pokémon where possible. Absorbs Electric moves, raising Special Attack one stage.",
        "Negates all effects of weather, but does not prevent the weather itself.",
        "Strengthens moves of 60 base power or less to 1.5× their power.",
        "Raises Special Attack by two stages upon having any stat lowered.",
        "Redirects single-target electric moves to this Pokémon where possible. Absorbs Electric moves, raising Special Attack one stage.",
        "Increases the same-type attack bonus from 1.5× to 2×.",
        "Raises Speed one stage upon being hit by a dark, ghost, or bug move."
    )

    val listData: ArrayList<Pokemon>
    get() {
        val list = arrayListOf<Pokemon>()
        for (position in pokemonNames.indices) {
            val pokemon = Pokemon()
            pokemon.name = pokemonNames[position]
            pokemon.detail = pokemonDetails[position]
            pokemon.photo = pokemonImages[position]
            pokemon.type = pokemonTypes[position]
            pokemon.height = pokemonHeights[position]
            pokemon.weight = pokemonWeights[position]
            pokemon.ability = pokemonAbilities[position]
            pokemon.ability_detail = pokemonAbilitiesDetail[position]
            pokemon.ability2 = pokemonAbilities2[position]
            pokemon.ability2_detail = pokemonAbilities2Detail[position]
            list.add(pokemon)
        }
        return list
    }
}